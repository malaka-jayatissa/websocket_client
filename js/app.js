var ws;

var client;

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
}

function connect() {
	console.log("Connect");
	var url =  $("#url").val();
	console.log(url);
	client = Stomp.client(url);
	client.heartbeat.outgoing = 10000;
	client.connect("User1","pass123",(frame) =>
		{
		console.log("Sucess " + frame);
		},
		(frame) =>
		{
			console.log("Error" + frame)
		}
	
	)
	
	
	/*ws.onmessage = function(data) {
		helloWorld(data.data);
	}
	*/
	//setConnected(true);
}

function disconnect() {
	if (ws != null) {
		ws.close();
	}
	setConnected(false);
	console.log("Websocket is in disconnected state");
}

function sendData() {
	var data = $("#user").val();
	

	var topic =  $("#topic").val();
	console.log(data);
	console.log(topic);

	client.send(topic,{},data);
}

function helloWorld(message) {
	$("#helloworldmessage").append("<tr><td> " + message + "</td></tr>");
}

$(function() {
	$("form").on('submit', function(e) {
		e.preventDefault();
	});
	$("#connect").click(function() {
		connect();
	});
	$("#disconnect").click(function() {
		disconnect();
	});
	$("#send").click(function() {
		sendData();
	});
});
